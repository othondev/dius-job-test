const Checkout = require('../src/coding-tests')
const assert = require('assert');
const {rule1,rule2,rule3} = require('../src/rules')

describe('Examples scenarios', function() {
  let checkout
  beforeEach( () => {
    checkout = new Checkout([rule1,rule2,rule3])
  })

  describe('We are going to have a 3 for 2 deal on Apple TVs', function() {
    it('should return 219', function() {
      checkout.scan('atv, atv, atv, vga')
      assert.equal(checkout.total(), 249)
    });
  });
  describe('the brand new Super iPad will have a bulk discounted applied', function() {
    it('should be 2718.95', function() {
      const total = checkout.scan('atv, ipd, ipd, atv, ipd, ipd, ipd')
      assert.equal(checkout.total(), 2718.95)
    });
  });
  describe('we will bundle in a free VGA adapter free of charge with every MacBook Pro sold', function() {
    it('should be 1949.98', function() {
      const total = checkout.scan('mbp, vga, ipd')
      assert.equal(checkout.total(), 1949.98)
    });
  });
  describe('When I have more vga than mbp', function() {
    it('should be 2009.98', function() {
      const total = checkout.scan('mbp, vga, vga, vga, ipd')
      assert.equal(checkout.total(), 2009.98)
    });
  });
  describe('When I have 12 atv', function() {
    it('should be 876', function() {
      const total = checkout.scan('atv, atv, atv, atv, atv, atv, atv, atv, atv, atv, atv, atv')
      assert.equal(checkout.total(), 876)
    });
  });
  describe('When I try the 3 rules', function() {
    it('should be 3648.95', function() {
      const total = checkout.scan('atv, atv, atv, ipd, ipd, ipd, ipd, vga, vga, mbp')
      assert.equal(checkout.total(), 3648.95)
    });
  });
});
