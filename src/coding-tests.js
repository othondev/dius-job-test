const itemStore = require('../data/itens.json')
module.exports = class Checkout {
  constructor(rules){
    this.rulesChain = rules
    this.index = 0
    this.itens = []
    this.discont = 0
  }
  scan(paramItens){
    this.itens = typeof paramItens === 'string' ?
      paramItens.split(',').map(i => { return {sku: i.trim()}}) :
      [ paramItens ] 
  }
  total() {
    const disc = this._getDiscont()
    const value = this.itens.reduce 
    (
      (a,c) => a+=new Number(itemStore[c.sku].price), 0
    )

    return value - disc
  }
  _getDiscont(){
    this.rulesChain[this.index](this.itens, this.next.bind(this))
    return this.discont   
  }
  next(disc) {
    this.index++
    this.discont += disc
    if(this.index < this.rulesChain.length){
      this._getDiscont()
    }
  }
}
