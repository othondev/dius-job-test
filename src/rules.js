const itemStore = require('../data/itens.json')

module.exports.rule1 = (itens, next) => {
  const countATV = itens.filter( e => e.sku === 'atv').length  
  const discont = Math.floor(countATV/3)*itemStore['atv'].price
  return next(discont)
}
module.exports.rule2 = (itens, next) => {
  const countIPD = itens.filter( e => e.sku === 'ipd' ).length
  const discont = countIPD >= 4 ? 50*countIPD : 0
  return next(discont)
}
module.exports.rule3 = (itens, next) => {
  const { vga, mpb } = itens.reduce( (a,c) => {
    if(c.sku === 'mbp')
      a.mpb++
    else if(c.sku === 'vga')
      a.vga++
    return a
  },{mpb:0,vga:0})
  const countItemDiscont = vga>=mpb ? mpb : vga
  const discont = countItemDiscont*itemStore['vga'].price
  return next(discont)
}

